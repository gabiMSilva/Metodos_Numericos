#Gabriela Medeiros da Silva
#16/0121817

import numpy as np
from matplotlib import pyplot as plt
import math

def Eq(w):
    som = 0
    for i in range(1, 20):
        som = som + (math.sin(w*ts[i]) - ys[i])**2
    return som

XX = []
YY = []
w0 = 0.1
w1 = 0.15

ys = [
       0.2493938 ,  0.72091586,  1.08935177,  0.82557285,  0.15184373,
      -0.55819513, -0.84832015, -1.13075724, -0.66132559, -0.37482053,
       0.49910671,  0.99533713,  0.97493417,  0.56770796, -0.59740919,
      -0.76333201, -1.05362856, -0.8085086 , -0.32029816,  0.4021104]
ts = [
        0.0        ,    5.26315789,   10.52631579,   15.78947368,
        21.05263158,   26.31578947,   31.57894737,   36.84210526,
        42.10526316,   47.36842105,   52.63157895,   57.89473684,
        63.15789474,   68.42105263,   73.68421053,   78.94736842,
        84.21052632,   89.47368421,   94.73684211,   100]

A = 0.1
B = 0.15

for i in range(12):
    xi1 = A + (B - A) / 3
    xi2 = B - (B - A) / 3

    xi = min(xi1, xi2)

    print('%.2d: W = %.4f, EQ(W) = %.4f' % (i, xi, Eq(xi)))

    if Eq(xi1) < Eq(xi2):
        B = xi2
    else:
        A = xi1

    if (i==19):
        print ("W final = %.4f" %(xi))

# Obs.: Último commit foi dado depois do prazo porque a internet da UnB não estava funcionando 
