import numpy as np
from matplotlib import pyplot as plt

def f(t):
    return np.exp(-t**2)

x = float(input("Escreva o valor de x: "))
h = x/10

wi = [
	 128/225, 
	 ((322 + 13*np.sqrt(70))/900),
	 ((322 + 13*np.sqrt(70))/900),
	 ((322 - 13*np.sqrt(70))/900),
	 ((322 - 13*np.sqrt(70))/900)
	 ]

ui = [
	  0, 
	  13/(np.sqrt(5-2*np.sqrt(10/7))),
	  -13/(np.sqrt(5-2*np.sqrt(10/7))),
	  13/(np.sqrt(5+2*np.sqrt(10/7))),
	  -13/(np.sqrt(5+2*np.sqrt(10/7)))
	  ]

res = 0

res = np.sum(
   h/2 *(wi[0]*f(x + h/2*(1 + ui[0]))) +
		(wi[1]*f(x + h/2*(1 + ui[1]))) +
		(wi[2]*f(x + h/2*(1 + ui[2]))) +
		(wi[3]*f(x + h/2*(1 + ui[3]))) +
		(wi[4]*f(x + h/2*(1 + ui[4])))
		)

print (res)
