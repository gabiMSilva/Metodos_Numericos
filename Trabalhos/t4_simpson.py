import numpy as np
from matplotlib import pyplot as plt

def f(t):
    return np.exp(-t**2)

x = float(input("Escreva o valor de x: "))

h = x/10
res = 0

for i in np.arange (0, x, h):
    x_i = i
    x_f = i + h
    x_m = (x_i+x_f)/2
    res = res + h/6*(f(x_i) + f(x_f) + 4*f(x_m))

res = res * (2/(np.pi**(1/2)))
print (i, ":", res)
