#Gabriel Martins de Castro 16/0121213
#Gabriela Medeiros da Silva 16/0121817

import numpy as np
from scipy import special
from matplotlib import pyplot as plt
from math import *

def X(w):
    return exp(w) * w

def W(x):
    a = 0
    b = x
    while (abs(b-a) > 1e-5):
        c = (a+b)/2
        if (X(c) == x):
            wf = c
            break
        else:
            if (X(a) < 0 and X(c) < 0):
                a = c
            elif (X(a) > 0 and X(c) > 0):
                a = c
            elif (X(b) < 0 and X(c) < 0):
                b = c
            elif (X(b) > 0 and X(c) > 0):
                b = c
            wf = (a+b)/2
    return wf

x = float(input("Insira o x0:"))
if (x < 0):
    while (x < 0):
        print("O x deve ser maior que 0")
        x = float(input("Insira um novo x0:"))
W(x)
