from math import *
import matplotlib.pyplot as pyplot
import numpy as np
from random import *

# Retorna a derivada de f(x) usando uma aproximação do limite com
# um determinado valor de epsilon.

# Derivada por definição: f'(x) = lim (f(x+epsilon)-f(x)/f(epsilon))
def derivada (f, epsilon = 1e-6):
    def df(x):
        return (f(x+epsilon)-f(x)/f(epsilon))

#xtol: variação mínima em x entre duas iterações
#ytol: tolerância em f(x)
# O algorítmo avança por no máximo n iterações e interrompe quando a primeira
#condições for satisfeita:
#1) o número máximo de iterações for atigido
#2) o valor de f(x) ficou menor em valor absoluto que ytol
#3) o deslocamento de x entre duas iterações sucessivas é menor que xtol

def newton (x0, f, df=None, n=10, xtol=1e-10, ytol=1e-10):
    if df == None:
        df = derivada(f)
    x = x0
    for i in range (n):
        x = x-f(x) / df(x)
        if abs(f(x)) < ytol:
            return x
        return x

x = randint(1 , 10)
f = sin
df = cos

x = newton (x, f, df)

print ('correto: ', pi)
print ('x:', x)
