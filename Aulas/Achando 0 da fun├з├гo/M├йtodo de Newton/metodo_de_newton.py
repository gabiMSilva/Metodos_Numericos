# Método de Newton(para achar o zero da função)

# 1. Um chute aleatório inicial de um ponto;
# 2. Calcula a reta tangente desse ponto e acha a raiz dessa reta;
# 3. Acha o valor correspondente dessa raiz na função inicial;
# 4. Repete o passo 2 e 3 n vezes até encontrar a raiz.

from math import *
from random import *

f = sin
df = cos

x = randint(1 , 10) #Seleciona um númerio aleatório inteiro entre 1 e 10
n = 10

for i in range (n):
    x = x - f(x) / df(x)
    print ('resultado:', x, 'f(x):', f(x))

print ('correto: ', pi)
