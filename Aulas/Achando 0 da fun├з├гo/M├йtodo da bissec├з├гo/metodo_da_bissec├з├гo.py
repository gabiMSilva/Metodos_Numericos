#Método da Bissecção(encontrar as raízes de uma função quadrática):
#   1.testar se a função tem um intervalo válido[a, b];
#       1.1. Se contém f(x) = 0;
#       1.2. Se o sinal de f(a) != f(b);
#       1.3. Difidir a função no meio e retornar aos passos 1 e 2;

#Exemplo: Achar as raízes da função sen(x/2);

#Sabemos que x' = pi logo, os valores iniciais serão [3, 4];

from numpy import sin, cos

a, b, c = 3, 4, 7
fa, fb, fc = cos(a/2), cos(b/2), cos(c/2)

for i in range(100):
    c = (a+b)/2
    fc = cos(c/2)

    if fc < 0:
        b = c
        fb = cos(b/2)

    elif fc > 0:
        a = c
        fa = cos(a/2)

if fa > (-fb):
    raiz = b
elif fa < (-fb):
    raiz = a

print ('A raíz da função é:', raiz)
