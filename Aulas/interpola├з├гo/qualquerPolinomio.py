# <y.fk(x)> - SOM (de k=1 até K) Ck' <fk(x).fk'(x)> = 0


import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-10, 10, 5)
n = len(x)
y = 3 * np.cos(x) + np.random.normal(size = n)

degree = 2

F = np.zeros([degree+1, n])
for i in range (degree + 1):
    F[i] = x**i

M = np.zeros([degree + 1, degree + 1])
for k in range (degree + 1):
    for l in range (degree + 1):
        M[k, l] = np.mean(F[k] * F[l])

b = np.zeros([degree+1])

for k in range(degree+1):
    b[k] = np.mean(y * F[k])

c = np.linalg.solve(M, b)
xx = np.linspace(-10, 10, 100)
yy = sum (c[i] * xx**i for i in range(degree + 1))

plt.plot(x, y, 'o')
plt.plot(xx, yy)
plt.show()
