# <y.fk(x)> - SOM (de k=1 até K) Ck' <fk(x).fk'(x)> = 0
# k=1: <y> - C1.<1>(1)- C2.<x> - C3.<x²> = 0
# k=2: <yx> - C1<x> - C2<x²> - C3<x³> = 0
# k=3: <y.x²> - C1<x²> - C2<x³> - C3<x⁴> = 0
#
#| 1   <x>   <x²>|  |C1|     |<y>  |
#|<x>  <x²>  <x³>|  |C2|  =  |<yx> |
#|<x²> <x³>  <x⁴>|  |C3|     |<yx²>|
#
# 1. Calcula as médias
# 2. Substitui na matriz M e vetor b [Mx = b]
# 3. Resolve o sistema linear(np.linalg.solve)
# 4. Faz um gráfico [y=C1 + C2x + C3x²]

def medx(x):
    return x.mean()

import matplotlib.pyplot as plt
import numpy as np

x = np.array ([-2, -1, 0, 1, 2])
y = np.array ([5.1, 2.1, 1, 1.9, 5.2])

M = np.array([
    [1, medx(x), medx(x**2)],
    [medx(x), medx(x**2), medx(x**3)],
    [medx(x**2), medx(x**3), medx(x**4)],
])

b = np.array([
    [medx(y)],
    [medx(y*x)],
    [medx(y*x**2)],
])

c1,c2,c3 = np.linalg.solve(M,b)

plt.plot(x, y, 'o')

xx = np.linspace(-2, 2, 100)
yy = c1 + c2*xx + c3*xx**2

plt.plot(xx, yy)
plt.show()
