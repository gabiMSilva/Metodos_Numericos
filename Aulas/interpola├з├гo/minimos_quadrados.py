#                           Técnica dos Mínimos Quadrados
#
#   Achamos as parábolas que passem perto dos pontos
#   Medimos a distância na vertical e somamos todas elas
#   A parábola que tiver a menor soma das distância será a parábola escolhida
#   E(a, b) = SOMATÓRIO(de i=0 até n) de yi-axi²-bxi-c
#   Acha o mínimo de E
#   dE/da = 0 (iguala a zero)
#   dE/db = 0
#   ^^^^^Muito Trabalho^^^^^
#   Ao invés de achar o somatório da distância, fazemos o somatório das distâncias ao quadrado(²)
#   X²(a, b) = SOMATÓRIO(de i=0 até n) de (yi-axi²-bxi-c)²
#   dX²/da = 0
#   dX²/db = 0
#   dX²/dc = 0
#
#   dX²/dC = SOMATÓRIO 2*(yi - f(xi, o))(-1)gk(xi = 0;


#                              Ajuste linar
#   Cria vetores x e y
#   Calcula cov(x, y) = <xy> - <x><y>
#       <x> = x.mean()
#   Calcula var(x) = <x²> - <x>²
#   Calcula b = cov(x,y)/var(x)
#   Calcula a = <y> - b<x>
def cov(x, y):
    return ((x*y).mean()-x.mean()*y.mean())

def var(x):
    return ((x**2).mean() - (x.mean())**2)

import numpy as np
from matplotlib import pyplot as plt

x = np.array([0, 1, 2, 3, 4, 5])
y = np.array([1.5, 2, 2.9, 4.1, 4.9, 6.2])

b = cov(x, y)/var(x)
a = y.mean() - b*(x.mean())

plt.plot(x, y, 'o')

xx = np.linspace(0, 5, 100)
yy = a*xx + b

plt.plot(xx, yy)
plt.show()
