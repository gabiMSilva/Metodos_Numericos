#       Polinômios de Lagrange
#   p(x) = a + bx + cx² + dx³
#   {1, x, x², x³} -> base
#   substituir x e y dos pontos na equação
#   pontos: (x1, y1), (x2, y2), (x3, y3), (x4, y4)
#   Montar um polinômio tal que:
#       L1(x) -> L1(x1) = 1
#                L1(xi != x1) = 0;
#   Se esse polinômio existir (^):
#   p(x) = y1L1(x) + y2L2(x) + y3L3(x) + y4L4(x).
#   Para x3: L1, L2, L4 = 0, logo:
#   p(x) = y3L3

#   pontos: (0,1)(1,2)(2,5)
#   Li(x) = Produtório de j=1 quando j!=i até n(grau do polinômio)= (x-xj)/(xi-xj)
#   L1 = (x-1)/(0-1) * (x-2)/(0-2) = x²/2 - 3x/2 + 1
#   L2 = (x-0)/(1-0) * (x-1)/(1-2) = -x² + 2x
#   L3 = (x-0)/(2-0) * (x-1)/(2-1) = x²/2 - x/2
#
#   p(x) = x²/2 - 3x/2+ 1 - 2x² + 4x + 5x²/2 - 5x/2
