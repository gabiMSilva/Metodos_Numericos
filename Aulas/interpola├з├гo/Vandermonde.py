#Interpolação polinomial de lagranger
#
#   Um conjunto de pontos e o objetivo é 'juntá-los' em um só segmento(função);
#   A primeira forma é encontrar uma reta a cada dois pontos que passe pelos mesmos;
#   Para encontrar uma reta que passe por dois pontos:
#       y-y1 = a . (x-x1);
#       = y-y1 = (y2-y1)/(x2-x1) . (x-x1)
#   Manualmente usar:
#       y = a + bx + cx² + dx³
#       substituir x e y pelo x e pelo y de cada ponto que você quer
# juntar e resolver por sistema;

import numpy as np
import matplotlib.pyplot as plt

pts = np.array([[0, 5], [0.5, 4], [1, 3], [2, 2], [3, 1]])
X, Y = pts.T #Transposta da matriz pts

plt.plot(X, Y, 'o')
plt.show()

b = np.array([2, 3, 2, 3, 2]);
M = np.array([
    [1**0, 1**1, 1**2, 1**3]
    [2**0, 2**1, 2**2, 2**3]
    [3**0, 3**1, 3**2, 3**3]
    [4**0, 4**1, 4**2, 4**3]
    [5**0, 5**1, 5**2, 5**3]
])
