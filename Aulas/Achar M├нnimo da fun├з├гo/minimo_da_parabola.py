# Achando o mínimo da função

#    1ª Forma:
#   Igualar a derivada a zero e achar os pontos críticos: máximos e mínimos;
#   x'=x - (f'(x)/f"(x)), onde o sugundo termo é o mínimo de f(x);

#    2ª Forma:
#   Chutar um ponto aleatório na parábola e achar outra parábola que melhor
# aproxime f(x) nesse ponto;
#   Para achar essa parábola:
#       f(x) = f(x0) + f'(x0)(x-x0)+1/2!*f"(x0)(x-x0)²;
#       f'(x)= f'(x0)+f"(x0)(x-x0);
#       -f'(x0)/f"(x0) = x-x0;
#       x' = x - f'(x0)/f"(x0);
#   Achar o mínimo dessa parábola;
#   Fazer isso n vezes;
#   Gradiente = Derivada primeira;
#   Hessiana = Derivada segunda;

#1ª Forma:

from sympy import symbols

x = symbols ('x')

def f(x):
    return -1/x**6 + 1/x**12

def df(x):
    return 6/x**7 - 12/x**13

def ddf(x):
    return 6*(-7 + 26/x**6)/x**8

x = 10
for i in range(20):
    x = x-df(x)/abs(ddf(x))
    print(x)
